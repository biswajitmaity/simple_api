const mongoose = require('mongoose');



const Schema = mongoose.Schema;


const userSchema = new Schema({
    name: { type: String, default: '' , require: true, trim: true},
    email: { type: String, default: '' , require: true, trim: true, unique: true },
    password: { type: String, default: '' , require: true, trim: true},
    products: [
      {
        name : { type: String, default:"", trim: true},
        availablefrom : { type: Date, default: Date.now,},
        availableto : { type: Date, default: Date.now,},
      }
    ],
    //date: { type: Date, default: Date.now }s,
    //buff: Buffer
  });
  let SchemaData= mongoose.model("User", userSchema);

  module.exports = SchemaData
 