const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const products = new Schema({
  user_id: { type: mongoose.ObjectId},
  pro_name: { type: String, default: '', require: true, trim: true },
  pro_description: { type: String, default: '', require: true, trim: true },
  //date: { type: Date, default: Date.now }s,
  //buff: Buffer
});
let productSchema = mongoose.model("products", products);

module.exports = productSchema
