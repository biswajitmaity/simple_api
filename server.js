
const express = require('express'); 
const mongoose = require('mongoose'); 
const passport = require('passport');
const app = express();  
const bodyParser = require('body-parser');  
const adminRoute = require('./routes/userRoute');
const product = require('./routes/productRoute');

// Create application/x-www-form-urlencoded parser  
app.use( bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/admin', adminRoute);
app.use('/product', product);



const connection = connect();

connection
  .on('error', console.log)
  .on('disconnected', connect)
  .once('open', listen);

module.exports = {
  app,
  connection
};

function listen() {
    const port = process.env.PORT || 3000;
  if (app.get('env') === 'test') return;
  app.listen(port);
  console.log('Express app started on port ' + port);
}

function connect() {

    const path= "mongodb+srv://biswajit:biswajit@cluster0-c9oyc.mongodb.net/test";
const options=  { 
    keepAlive: 1, 
    useNewUrlParser: true,
    useUnifiedTopology: true
};

  mongoose.connect(path, options);
  return mongoose.connection;
}
