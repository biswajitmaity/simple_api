let jwt = require('jsonwebtoken');
const secretToken = require('./configAuth');

let checkToken = (req, res, next) => {
console.log("Enter into token verify.");

  if (req.headers.token) {
    jwt.verify(req.headers.token, secretToken, (err, decoded) => {
      if (err) {
          console.log("token error  === ",err);
        return res.status(500).send({ 
          auth: false, message: 'Failed to authenticate token.' 
        });
      } else {
        console.log("token verify  === ",decoded);

          next();
      }
    });
  } else {
    console.log("token is not supplied  === ");
    return res.status(500).send({ 
      auth: false, message: 'token is null.' 
    });
  }
};

module.exports =  checkToken;