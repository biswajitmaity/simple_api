const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const saltRounds = 10;
const secretToken = require('../authentication/configAuth');
//const checkToken = require('../authentication/authToken');
const SchemaData = require('../schema/userSchema');
const mail = require('../Send_Mail/sendMail');
const Message = require('../Send_Message/send_message');

let userControle = {};
let Token;

// ------------- Create new user --------------
userControle.createData = (req, callback) => {
  const body = req.body;
  bcrypt.hash(body.password, saltRounds, (err, hash) => {


    let name = body.name;
    let email = body.email;
    let password = hash;
    let products = body.products;


    if (!email) {
      callback({
        status: 401,
        message: "Email ID is required"
      }, 401)
    } else if (!name) {
      callback({
        status: 401,
        message: "Name is required"
      }, 401)
    } else {
      const addData = {
        name,
        email,
        password,
        products
      }
      const newData = new SchemaData(addData);
      newData.save((err, res) => {
        if (!err) {
          callback({
            status: 200,
            message: "New user creacted sucessfully",
            response: res
          }, 200)
        } else if (err.errmsg) {
          callback({
            status: 500,
            message: "user already creacted ",
            response: err
          }, 500)
        }
        else {
          callback({
            status: 500,
            message: "New user is not creacted ",
            response: err
          }, 500)
        }

      })
    }
  });
}

// ----------- Find user data ---------------
userControle.findData = (req, callback) => {
  // console.log(req);
  const query = req.query;
  let email = query.email;
  console.log(query.email);

  if (email) {
    let emailObj = {
      "email": query.email
    };
    SchemaData.find(emailObj).exec((err, res) => {
      console.log(res, "res");

      if (!err) {
        callback({
          status: 200,
          message: "Find sucessfully.",
          response: res[0]
        }, 200)
        //console.log(res);

      }
      else {
        callback({
          status: 500,
          message: "Can't find user data.",
          response: res[0]
        }, 500)
      }

    })
  }
  else {
    callback({
      status: 404,
      message: "Email id is empty.",
      response: res
    }, 404)
  }

}

// ---------------- Update user data ----------------
userControle.updateData = (req, callback) => {

  let body = req.body;
  console.log(body.email);
  bcrypt.hash(body.password, saltRounds, (err, hash) => {
    if (!body.email || typeof body.email == undefined) {
      console.log("check email  :  ", body.email);

      callback({
        status: 404,
        message: "Something wrong.",
        response: res
      }, 404)
    } else {
      console.log("Enter into update fatch");

      let updateUser = {
        name: body.name,
        email: body.email,
        password: hash
      };
      SchemaData.updateOne({ email: body.email }, { $set: updateUser }, (err, res) => {
        console.log('Enter into updata fatch of fatabase  === ', err, res);

        if (!err) {
          callback({
            status: 200,
            message: "Update sucessfully.",
            response: res
          }, 200)
        } else {

          callback({
            status: 500,
            message: "user is not updated.",
            response: res
          }, 500)
        }
      });
    }
  })
}

// ----------- Delete user data ----------------
userControle.deleteData = (req, callback) => {
  console.log("delete email user", req.body.email);
  let body = req.body;
  if (!body.email || typeof body.email == undefined) {
    callback({
      status: 404,
      message: "Email id undefined for delete.",
      response: res
    }, 404)
  } else {
    let ObjectData = {
      "email": body.email
    }
    SchemaData.deleteOne((ObjectData), (err, res) => {
      console.log("Enter into delete function");
      if (!err) {
        callback({
          status: 200,
          message: "User deleted sucessfully.",
          response: res
        }, 200)
      } else {
        callback({
          status: 500,
          message: "User is not deleted.",
          response: res
        }, 500)
      }
    })
  }
}

// ------------ Login user -----------------
userControle.userLongin = (req, callback) => {
  const body = req.body;
  if (body.email && body.password) {
    let ObjEmail = {
      email: body.email
    };
    SchemaData.find(ObjEmail).exec((err, res) => {
      //console.log("Enter into login ", res, err);
      if (res.length > 0) {
        //console.log(res[0].password);
        bcrypt.compare(body.password, res[0].password, (errData, result) => {
          console.log(result);
          if (result) {
            Token = jwt.sign({ email: body.email }, secretToken, { expiresIn: 60 * 60 });
            console.log("Token  ==================",Token);
           // mail();
           Message ();
            callback({
              status: 200,
              message: "User login sucessfully.",
              response: res,
              token: Token
            }, 200)
          } else {
            callback({
              status: 500,
              message: "Worng password.",
              response: err
            }, 500)
          }
        })
      } else {
        callback({
          status: 500,
          message: "Invalid email id.",
          response: err
        }, 500)
      }
    })
  }
}

// ---------------- Add products into user product -----------------
userControle.addProduct = (req, callback) => {
  console.log("Enter into product add", req.body);
  let body = req.body;
  if (body.id) {
    let idObj = {
      "_id": body.id
    };
    let emailObj = {
      "_id": body.email
    };
    console.log("enter ======");

    SchemaData.find(idObj).exec((err, res) => {
      if (!err) {
        //     console.log(res);
        SchemaData.updateOne(idObj, { $push: { products: body.products } }, (err, res) => {
          if (!err) {
            callback({
              status: 200,
              message: "Products Update sucessfully.",
              response: res
            }, 200)
          } else {

            callback({
              status: 500,
              message: "user product is not updated.",
              response: res
            }, 500)
          }
        })
      }
    })
  }
  else {
    callback({
      status: 404,
      message: "Product is not valid",

    }, 404)
  }
}

// --------------- Updata only product name field into user product ------------------
userControle.updateProducts = (req, callback) => {
  console.log("Enter into update product =====  ", req.body);
  const body = req.body;
  if (!body.userId && !body.productId) {
    callback({
      status: 404,
      message: "Product is not valid.",
      response: err
    })
  } else {
    const objectUserId = {
      "_id": body.userId
    };
    SchemaData.findOne(objectUserId).exec((err, res) => {
      if (!err) {
        const objectProductId = {
          "_id": body.productId
        };
        SchemaData.findOne(objectProductId).exec((err, res) => {
          if (!err) {

            SchemaData.updateOne({ "products._id": body.productId, "_id": body.userId }, { $set: { "products.$.name": body.productsName } }, (err, res) => {
              if (!err) {
                console.log(res);

                callback({
                  status: 200,
                  message: "Update product sucessfully.",
                  response: res
                }, 200)
              } else {
                callback({
                  status: 500,
                  message: "Product is not updated.",
                  response: err
                })
              }
            })

          } else {
            callback({
              status: 500,
              message: "Product id is not valid.",
              response: err
            })
          }
        })
      } else {
        callback({
          status: 500,
          message: "User id is not valid.",
          response: err
        })
      }
    })
  }
}

// ---------------- Delete products into user product ----------------
userControle.deleteProducts = (req, callback) => {
  console.log("Enter into update product =====  ", req.body);
  const body = req.body;
  if (!body.userId && !body.productId) {
    callback({
      status: 404,
      message: "Product is not valid.",
      response: err
    })
  } else {
    const objectUserId = {
      "_id": body.userId
    };
    SchemaData.findOne(objectUserId).exec((err, res) => {
      if (!err) {
        const objectProductId = {
          "_id": body.productId
        };
        SchemaData.findOne(objectProductId).exec((err, res) => {
          if (!err) {

            SchemaData.update({ "products._id": body.productId }, { $pull: { products: { _id: body.productId } } }, (err, res) => {
              if (!err) {
                console.log(res);
                callback({
                  status: 200,
                  message: "product delete sucessfully.",
                  response: res
                }, 200)
              } else {
                callback({
                  status: 500,
                  message: "Product is not delete.",
                  response: err
                })
              }
            })

          } else {
            callback({
              status: 500,
              message: "Product id is not valid.",
              response: err
            })
          }
        })
      } else {
        callback({
          status: 500,
          message: "User id is not valid.",
          response: err
        })
      }
    })
  }
}

// --------------- Add and update new product field data into user product ---------------
userControle.addProductFileds = (req, callback) => {
  console.log("Enter into product add", req.body);
  let body = req.body;
  if (body.userId && body.productId) {
    let idObj = {
      "_id": body.userId
    };
    let productIdObj = {
      "_id": body.productId
    };

    console.log("enter ======");

    SchemaData.find(idObj).exec((err, res) => {
      if (!err) {
        //     console.log(res);
        SchemaData.find(productIdObj).exec((err, res) => {
          if (!err) {
            // let NewProductFieldAdd = new SchemaData();
            SchemaData.updateOne({ "_id": body.userId, "products._id": body.productId }, { $set: { "products.$.name": body.name, "products.$.availablefrom": body.availablefrom, "products.$.availableto": body.availableto } }, (error, Response) => {
              console.log("Error  ==  ", error, "\nResponse  ====  ", Response);

              if (!error) {
                callback({
                  status: 200,
                  message: "Products Update sucessfully.",
                  response: res
                }, 200)
              } else {
                callback({
                  status: 500,
                  message: "user product is not updated.",
                  response: res
                }, 500)
              }
            })
          } else {
            callback({
              status: 500,
              message: "user product field id is not valid.",
              response: res
            }, 500)
          }
        })
      } else {
        callback({
          status: 500,
          message: "user product id is not valid.",
          response: res
        }, 500)
      }
    })
  }
  else {
    callback({
      status: 404,
      message: "Product is not valid",

    }, 404)
  }
}

// --------------- Api call before Token permission check ---------------- 
userControle.tokenPermission = async (req, callback) => {

  callback({
    status: 200,
    message: "Api sucess base on token"
  })

}

// ------------------ Find products depen on available date into user products ------------------
userControle.FindProductsOnDate = (req, callback) => {
  console.log("Enter into update product =====  ", req.body);
  const body = req.body;
  if (!body.id) {
    callback({
      status: 404,
      message: "Product is not valid.",
      response: err
    })
  } else {
    const objectUserId = {
      "_id": body.id
    };
    SchemaData.findOne(objectUserId).exec((error, response)=>{
      //console.log(response);
      let fromData = new Date(body.availablefrom);
      let toData = new Date(body.availableto);
    if(!error){
    // SchemaData.aggregate([
    //   {$group: 
    //      {_id: body.id} 
    //   }
    // ])
    SchemaData.findOne({_id:body.id}).populate("products.$")
    .exec((err, res) => {
      if (!err) {
        let response2=[],i=0;
        let Response1 = res.products.map((data) => {
          if(data.availablefrom >= fromData && data.availableto <= toData){
            response2[i]=data;
            i++;
            return data;
          }
          
        })
        console.log("fgghgh ==== ",res,"frehtgrg   =======   ",Response1,"yuftyjoi  ===== ",response2);

        callback({
          status: 401,
          message: "Find product on date sucessfully.",
          response: response2
        }, 401)
      } else {
        callback({
          status: 500,
          message: "Product is not find.",
          response: err
        })
      }
    })
  } else {
    callback({
      status: 500,
      message: "user is not find.",
      response: err
    })
  }
  })
  }
}

module.exports = userControle;



