const productSchema = require('../schema/ProductSchema');
const SchemaData = require('../schema/userSchema');

let productController = {};

productController.createProduct = async (req, callback) => {
    const body = req.body;
    console.log("jkjdhfgskj   ====  ", body);
    let user_id = body.user_id;
    console.log("=================user_id================>",(user_id))
    // console.log("=================user_id================>",JSON.parse(user_id));

    
    let pro_name = body.pro_name;
    let pro_description = body.pro_description;

    if(!user_id && !pro_name && !pro_description){
        callback({
            status : 404,
            message : "Fill up all the fields."
        }, 404)
    } else {
        const addNewData ={
            user_id,
            pro_name,
            pro_description
        };
        console.log("addNewData==>", addNewData);
        
    let SchemaNewProduct =new productSchema(addNewData);
    await SchemaNewProduct.save((err, res)=> {
        console.log("Enter into new product save ====   ",err,"\nResponse   ======     ",res);
        if(!err){
            callback({
                status: 200,
                message: "New product is successfully created.",
                response: res
            }, 200)
        } else if (err.errmsg) {
            callback({
                status: 500,
                message: "Already use this product.",
                response: err
            }, 500)
        } else {
            callback({
                status: 500,
                message: "New product is not created into database.",
                response: err
            }, 500)
        }
    })
 }
}

productController.productFindOnUserid = (req, callback) =>{
    //console.log(req.body);
    let body = req.body;
    SchemaData.aggregate([
        {
            $lookup: 
            {
                 from: "products", 
                 localField: "_id", 
                 foreignField: "user_id", 
                 as: 'orderdetails'
            }
        }]).exec((err, res) => {
console.log("=============View product data ==========> ",res);
let resData = [], i1 =0;
res.map((data) => {
    if(data._id == body.user_id) {
        data.orderdetails.map((data1) => {
            if(data1._id == body.product_id) {
                resData[i1]=data1;
                i1++;
            }
        }) 
    }
})

if(!err) {
    callback({
        status: 200,
        message: "Successfully find product data.",
        response: resData
    }, 200)
} else {
    callback({
        status: 500,
        message: "product data is not finded.",
        response: err
    }, 500)
}

    })
    
}

module.exports= productController;
