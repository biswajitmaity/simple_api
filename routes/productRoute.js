const express = require('express');
const router = express.Router();
const productController = require('../controller/productController');
const CheckToken = require('../authentication/authToken');

router.post('/productAdd', (req, res) => {    
    productController.createProduct(req, (response, code) => {
        res.status(code).send(response);
    })
});

router.post('/productData', (req, res) => {
    productController.productFindOnUserid(req, (response, code) => {
        res.status(code).send(response);
    })
})

module.exports= router;