const express = require('express');
const router = express.Router();

const userControle = require('../controller/userController');
//const authToken = require('../authentication/authToken');
const checkToken = require('../authentication/authToken');


router.post('/addUser', (req, res) => {
  userControle.createData(req, (response, code) => {
    res.status(code).send(response);
 })
});

router.get('/findUser/email', (req, res)=>{
  userControle.findData(req, (response, code)=>{
    res.status(code).send(response);
  })
});

router.post('/updateUser', (req, res) =>{
  userControle.updateData(req,(response) =>{
    res.send(response);
  })
});

router.post('/deleteUser', (req, res) =>{
  userControle.deleteData(req, (response, code)=>{
    res.status(code).send(response);
  })
});

router.post('/userlogin', (req, res) =>{
  userControle.userLongin(req,(response, code) =>{
    res.status(code).send(response);
  })
})

router.post('/TokenPermission', checkToken, (req, res, next) =>{
  userControle.tokenPermission(req, (response, code)=>{
    res.status(code).send(response);
  })
});

router.post('/productAdd', (req, res) =>{
  userControle.addProduct(req, (response)=>{
    res.send(response);
  })
});

router.post('/productUpdate', (req, res) =>{
  userControle.updateProducts(req, (response, code) =>{
    res.status(code).send(response);
  })
})

router.post('/productDelete', (req, res) => {
  userControle.deleteProducts(req, (response, code) => {
    res.status(code).send(response);
  })
})

router.post('/addProductFileds', (req, res) =>{
  userControle.addProductFileds(req, (response, code)=>{
    res.status(code).send(response);
  })
});

router.post('/findProductsOnDate', (req, res) =>{
  userControle.FindProductsOnDate(req, (response,code)=>{
    res.status(code).send(response);
  })
});

module.exports = router;


